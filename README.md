
sairem
======
European Spallation Source ERIC Site-specific EPICS module : sairem

This module provides device support for Magnetron and Automatic 4 stubs tuner (AI4S). The hardware documentation is available on folder docs/

Dependencies
============
modbus 3.0.0

Using the module
================

There are examples showing how to use the module on folder cmds/

Buildind the module
===================

This module should compile as a regular EPICS module.
* Run `make` from the command line.

This module should also compile as an E3 module:
* Install `conda` (https://docs.conda.io/en/latest/miniconda.html)
* Create and activate a conda environment that minimally includes the following packages: `make perl tclx compilers epics-base require`
* Run `make -f Makefile.E3` to build the target using the E3 build process
